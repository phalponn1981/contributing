Thanks for considering a contribution to GitLab's
[built-in project templates](https://docs.gitlab.com/ee/user/project/working_with_projects.html#create-a-project-from-a-built-in-template).

Full instructions for contributing can be found in our [Contributing to Project Templates instructions](https://docs.gitlab.com/ee/development/project_templates.html) page.
